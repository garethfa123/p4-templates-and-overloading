//
//  main.cpp
//  CommandLineTool
//
//  Created by Tom Mitchell on 08/09/2017.
//  Copyright © 2017 Tom Mitchell. All rights reserved.
//

#include <iostream>
#include "Array.h"

int max (int a, int b)
{
    if (a > b)
        return b;
    else return a;
}

double max (double a, double b)
{
    std::cout << "max for doubles\n";
    if (a < b)
      
        return b;
    else
        return a;
}

template <typename Type>
Type min (Type a, Type b)
{
    if (a < b)
        return a;
    else
        return b;
}


template <typename Type>
Type add (Type a, Type b)
{
    a = a + b;
    return a;
}

template <typename Type>
void print (Type array, int size)
{
    
    for (int i = 0; i < size; i++)
    {
        std::cout << "Array value = " << array.get(i);
    }
}

template <typename Type>
double getAverage (Type array, int size)
{
    double number = 0;
    for (int i = 0; i < size; i++)
    {
        std::cout << "Array value = " << array.get(i);
        number = number + array.get(i);
    }
    
    number = number / size;
    std::cout << "Average = " << number;
    return number;
}



int main()
{
    Array array;
    
    array.add(5);
    array.add(6);
    array.add(7);
    
    print(array, array.size());
    getAverage(array, array.size());
    
    std::cout << "value = " << array.get(2);
    
    if (max (0.0, 10.0) != 10)
    {
        std::cout << "max() does not work";
    }
    
    if (max (10.0, 0.0) != 10)
    {
        std::cout << "max() does not work";
    }
    
    if (max (10.0, 10.0) != 10)
    {
        std::cout << "max() does not work";
    }
    
    if (min (6, 7) != 6)
    {
        std::cout << "max() does not work";
    }
    
    
    
    return 0;
}
